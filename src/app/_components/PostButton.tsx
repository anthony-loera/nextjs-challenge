"use client";
import { ListItemButton } from "@mui/material";
import { useRouter } from "next/navigation";

interface Post {
  id: number;
  title: string;
}

export default function PostButton(post: Readonly<Post>) {
  const router = useRouter();
  const handleClick = (id: number) => () => {
    router.push(`/post/${id}`);
  };

  return (
    <ListItemButton onClick={handleClick(post.id)}>
      <p>{post.id}</p>
      {post.title}
    </ListItemButton>
  );
}
