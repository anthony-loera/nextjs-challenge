import { List, ListItem } from "@mui/material";
import PostButton from "./PostButton";

interface Posts {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export default async function PostMenu() {
  const response = await fetch("https://jsonplaceholder.typicode.com/posts");
  const results = await response.json();

  return (
    <List>
      {results?.map((p: Posts) => {
        return (
          <ListItem key={p.id}>
            <PostButton id={p.id} title={p.title} />
          </ListItem>
        );
      })}
    </List>
  );
}
