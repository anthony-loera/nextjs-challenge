import { Card, Typography } from "@mui/material";
import axios from "axios";
import Image from "next/image";

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}
interface Props {
  id?: string;
}

export default async function SinglePost({ id }: Readonly<Props>) {
  const post: Post = await axios
    .get(`https://jsonplaceholder.typicode.com/posts/${id ?? "100"}`)
    .then((results) => results.data);

  return (
    <Card
      sx={{
        width: 500,
        color: "#FFF",
        backgroundColor: "#000",
        boxShadow:
          "3px 3px 1px 2px #FFFAFA,3px 3px 1px -2px #FFFAFA,0px 3px 1px -2px #FFFAFA",
      }}
    >
      <Image
        src="https://picsum.photos/200/300"
        height={200}
        width={500}
        alt="cute picture"
      />

      <Typography variant="h5" sx={{ paddingBottom: 3 }}>
        {post.title}
      </Typography>
      <Typography variant="h6">{post.body}</Typography>
    </Card>
  );
}
