import styles from "./page.module.css";
import Header from "./Header";
import { Grid } from "@mui/material";
import PostMenu from "./_components/PostsMenu";
import SinglePost from "./_components/SinglePosts";

export default function Home() {
  return (
    <div>
      <Header />
      <Grid container className="postCard">
        <div className={styles.description}>
          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <PostMenu />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <SinglePost />
          </Grid>
        </div>
      </Grid>
    </div>
  );
}
