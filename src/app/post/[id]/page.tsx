import SinglePost from "@/app/_components/SinglePosts";
import styles from "@/app/page.module.css";
import { Grid } from "@mui/material";
import Header from "@/app/Header";
import PostMenu from "@/app/_components/PostsMenu";

interface Props {
  params: { id: string };
}

export default function Posts({ params }: Readonly<Props>) {
  return (
    <div>
      <Header />
      <Grid container className="postCard">
        <div className={styles.description}>
          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <PostMenu />
          </Grid>
          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <SinglePost id={params.id} />
          </Grid>
        </div>
      </Grid>
    </div>
  );
}
